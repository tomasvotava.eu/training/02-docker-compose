from typing import List
import pymongo
from pydantic import BaseModel
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

class Note(BaseModel):
    content: str
    author: str = "Tom"

client = pymongo.MongoClient("mongodb://fajnova_databaze:27017")
db = client.get_database("notes")
notes_collection = db.get_collection("notes")

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.get("/", response_model=List[Note])
async def list_notes():
    return [note for note in notes_collection.find()]

@app.post("/", response_model=Note)
async def create_note(note: Note):
    notes_collection.insert_one(note.dict())
